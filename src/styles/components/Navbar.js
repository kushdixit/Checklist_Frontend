import styled from 'styled-components'

export const NavBarContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
  margin: 0 auto;
  padding: 15px 10px 15px 15px;
  align-items: center;
  .right-menu {
    list-style: none;
    margin-bottom: 0;
    display: flex;
    height: 100%;
    align-items: center;
    li {
      position: relative;
      top: 0;
      cursor: pointer;
      color: #fff;
      .Icon {
        transition: all 0.1s ease-in-out;
      }
      .Icon:hover {
        transform: scale(1.16);
      }
    }
    .profileSection {
      img {
        transition: all 0.1s ease-in-out;
      }
      img:hover {
        transform: scale(1.16);
      }
    }
  }
  .notiFicationBell {
    cursor: no-drop !important;
  }
  .li-wrapper {
    margin: 0 15px;
    position: relative;
    padding-bottom: 10px;
  }

  .Icon {
    background: #393939;
    width: 50px;
    height: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    border-radius: 5px;
    box-shadow: 0px 3px 6px #00000029;
  }

  .profileSection {
    position: relative;
    display: inline-block;
    cursor: pointer;
    padding-bottom: 6px;
    margin: 0 0 0 12px;
  }
  .profileImage {
    border-radius: 50%;
  }

  .addContentDropdownSub {
    display: inline-block;
    cursor: pointer;
    display: flex;
    align-items: left;
    font-size: 14px;
  }
  .dropdown-content {
    display: none;
    position: absolute;
    top: 65px;
    right: 10px;
    min-width: 160px;
    color: #ffffff;
    background: #393939;
    box-shadow: 0px 3px 6px #00000029;
    border-radius: 8px;
    z-index: 99;
    border-bottom: 1px solid #fff;
  }

  .addDropdown-content {
    display: none;
    position: absolute;
    right: 0;
    min-width: 146px;
    color: #ffffff;
    top: 50px;
    background: #393939;
    box-shadow: 0px 3px 6px #00000029;
    border-radius: 16px;
    font-size: 14px;
    z-index: 5;
    border-bottom: 1px solid #fff;
    ul {
      list-style: none;
      li {
        position: relative;
        top: 0;
        left: 0;
      }
    }
  }

  #contacts {
    position: relative;
    .sub-menu {
      display: none;
    }
    :hover {
      .sub-menu {
        display: block;
        position: absolute;
        top: -52px;
        width: 100%;
        right: 100%;
        height: 100%;
        .DropdownContentSub:hover {
          opacity: 1;
        }
        .addDropdownContent:hover {
          opacity: 1;
        }
      }
    }
  }

  .addDropdown-content-main {
    display: flex;
    position: absolute;
    right: 0;
    min-width: 160px;
    color: #ffffff;
    top: 23px;
    background: #393939;
    box-shadow: 0px 3px 6px #00000029;
    border-radius: 16px;
    z-index: 1;
  }

  .dropdown-content .dropdownContent {
    color: #ffffff;
    font-size: 14px;
    padding: 12px 16px;
    text-decoration: none;
    color: #ffffff;
    display: block;
    cursor: pointer;
  }
  .addDropdown-content .dropdownContentAdd {
    color: #ffffff;
    font-size: 14px;
    padding: 12px 16px;
    text-decoration: none;
    color: #ffffff;
    min-width: 120px;
    display: block;
    cursor: pointer;
  }
  .addDropdown-content-sub {
    color: #ffffff;
    font-size: 14px;
    padding: 12px 16px;
    text-decoration: none;
    color: #ffffff;
    display: block;
    cursor: pointer;
  }
  .addDropdown-content-main {
    color: #ffffff;
    font-size: 14px;
    padding: 12px 16px;
    text-decoration: none;
    color: #ffffff;
    display: none;
    cursor: pointer;
  }
  .DropdownContentSub {
    display: inline-flex;
    border-bottom: 1px solid #fff;
    width: 195px;
  }
  .addDropdownContent {
    display: inline-flex;
    border-bottom: 1px solid #fff;
    width: 200px;
  }
  .addDropdownContentSub {
    display: inline-flex;
    border-bottom: 1px solid #fff;
    width: 200px;
  }
  .dropdownContentAdd:hover {
    opacity: 0.3;
  }
  /* .DropdownContentSub:hover {
    opacity: 0.3;
  }
  .addDropdownContent:hover {
    opacity: 0.3;
  } */
  .dropdown-content .dropdownContent:hover {
    background-color: #393939;
  }

  .addDropdown-content .addDropdownContent {
    color: #ffffff;
    font-size: 14px;
    text-decoration: none;
    display: block;
    cursor: pointer;
    border-bottom: 1px solid #fff;
    :last-child {
      border: none;
    }
  }

  .profileSection :hover .dropdown-content {
    background-color: #393939;
    display: block;
  }
  .addContentDropdown :hover .addDropdown-content {
    background-color: #393939;
    display: block;
  }
  .addContentDropdown2 :hover .addDropdown-content2 {
    background-color: #393939;
    display: block;
  }
  .addContentDropdownSub :hover .addDropdown-content-main {
    background-color: #393939;
    display: block;
  }
  .addDropdown-content-sub {
    background-color: #393939;
    display: block;
  }
`
export const NavLogo = styled.div`
  img {
    position: relative;
    left: 30px;
    cursor: pointer;
  }
`

export const DropImage = styled.div`
  width: 20px;
  height: 20px;
  padding: 6px 22px 6px 16px;
`
